package com.gl.capstone.ShopForHomeMain.remoteservice;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.gl.capstone.ShopForHomeMain.model.DiscountCoupon;
import com.gl.capstone.ShopForHomeMain.model.RequestToken;
import com.gl.capstone.ShopForHomeMain.model.UserDiscountCoupons;

public class RemoteDiscountService {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private RequestToken requestToken;
	private String serviceURL;
	
	public RemoteDiscountService() {
		// TODO Auto-generated constructor stub
	}

	public RemoteDiscountService(String serviceURL) {
		this.serviceURL = serviceURL.startsWith("http") ? serviceURL
				: "http://" + serviceURL;
	}
	
	public String assignCoupon(long couponId, long userid) {
		ResponseEntity<String> re = restTemplate.exchange(serviceURL+"/admin/setUserCoupons/"+couponId+"/"+userid,HttpMethod.POST , setToken(), String.class);
		return re.getBody();
	}
	
	public void deleteUserCoupon(long couponId) {
		restTemplate.exchange(serviceURL+"/removeUserCoupon/"+couponId,HttpMethod.DELETE, setToken(), Object.class);
	}
	
	public void deleteUserCoupons(long userId) {
		restTemplate.exchange(serviceURL+"/removeAllUserCoupon/"+userId,HttpMethod.DELETE, setToken(), Object.class);
	}
	
	public List<UserDiscountCoupons> getUserCoupons(long userId){
		UserDiscountCoupons[] coupons = restTemplate.exchange(serviceURL+"/getUserCoupons/"+userId,HttpMethod.GET, setToken(), UserDiscountCoupons[].class).getBody();
		return Arrays.asList(coupons);
	}
	
	public DiscountCoupon getCoupon(long redeemCode) {
		return restTemplate.exchange(serviceURL+"/getUserCoupon/"+redeemCode,HttpMethod.GET, setToken(), DiscountCoupon.class).getBody();
	}
	
	public HttpEntity<String> setToken() {
		HttpHeaders headers = new HttpHeaders();
		headers.setBearerAuth(requestToken.getToken());
		headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> entity = new HttpEntity<>(headers);
		return entity;
	}
}

 